//Task 1

let name = 'Oksana';
let admin = name;
console.log(admin);

//Task 2

let days = 8;
let hoursInDay = days * 24;
let minutesInDay = hoursInDay * 60;
let secondsInDay = minutesInDay * 60;
console.log(`There are ${secondsInDay} seconds in ${days} days`);

//Task 3

const userInput = prompt('Введіть будь-яке значення:');
console.log('Ви ввели:', userInput);
